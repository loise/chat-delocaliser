﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net; // pour utiliser les EndPoint
using System.Net.Sockets; //pour utiliser les sockets
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Threading;
using System.Xml.Serialization;

namespace Reasaut_reprise
{
    public partial class Chat_priver : Form
    {
        public string nom = "void"; //Le nom de l'interlocuteur
        public string ip; //ip de l'interlocuteur
        public bool serveur; //savoir  si est le serveur
        public int port =  5550; //savoir  si est le serveur
        public bool stop_thread = false;
        public string moi_ip;   //ip de la machine

        public Chat_priver(string a_nom_priver, string a_ip,int a_port,bool a_serveur)
        {
            InitializeComponent();
            labelNomPersonnePriver.Text = a_nom_priver;
            nom = a_nom_priver;
            ip = a_ip;
            serveur = a_serveur;
            port = a_port;

        }

        //Création d'un buffer pour la réception et l'envoi
        byte[] buffer;

        //création des sockets.
        //sckEcoute : permet d'attendre une demande de connexion
        public Socket sckEcoute = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        //Serveur : socket utiliser par le client pour communiquer (sckServeur du client = sckEcoute du serveur)
        public Socket sckClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        public Socket sckServeur = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);


        //le socket client ou serveur sera "copié" dans ce socket
        public Socket sck = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        //création des références pour les endPoint
        EndPoint epLocal, epDistant;

        private void buttonEnvoiPriver_Click(object sender, EventArgs e)
        {
            envoi_message();
        }

        public void starter_chat(string a_ip)
        {
            //stop_thread = true;
            if (serveur)
            {
                //Ecrit dans selon si ces le serveur ou le client
                listBox_chat_priver.Items.Add("Serveur");
                sck = sckClient;
            }
            else
            {
                serverveur_part(a_ip);
                listBox_chat_priver.Items.Add("client");
                sck = sckServeur;
            }
            listBox_chat_priver.Items.Add(ip);
            //Création d'un buffer de 1024 bytes pour la réception
            buffer = new byte[1024];
            if (!stop_thread)
            {
                try
                {
                    //Nous devons démarrer la réception asynchrone sur le socket. Si la réception n'est pas asynchrone,
                    //le CPU ne pourra pas faire autre chose. L'ordi va geler.
                    //options : endroit de réception (buffer), à quel endroit dans "buffer" commençons-nous la réception (0 = début),
                    //la longueur du data à recevoir (buffer.Length), les "flags" utilisés (ici, aucun),
                    //la fonction appelée une fois la réception effectuée,
                    //"state" : contient l'information passée en argument à la fonction AsyncCallBack (ici : buffer)

                    sck.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallBack), buffer);
                }
                catch(Exception e)
                {
                    throw new Exception(e.ToString());
                    listBox_chat_priver.Items.Add("Système : Erreur de réception. (starter_chat)");
                }
            }
        }

        //Crée les socket pour se connecter au serveur
        public void serverveur_part(string a_ip)
        {
            sckServeur = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //Création d'un Endpoint distant
            epDistant = new IPEndPoint(IPAddress.Parse(a_ip), port);
            try
            {
                //Établie la connexion avec le endpoint
                sckServeur.Connect(epDistant);
            }
            catch { }
        }

        private void envoi_message()
        {
            //Pour la conversion
            ASCIIEncoding aEncoding = new ASCIIEncoding();
            //Buffer d'envoi
            byte[] envoiData = new byte[1024];

            //transforme la string de la textBox Envoi en tableau d'octets
            envoiData = aEncoding.GetBytes(textBoxChatPriver.Text);

            try
            {
                //envoie du message sur le socket
                //En TCP, il faut indiquer la longueur pour la confirmation de réception et le contrôle de flux
                //Il faut également indiquer les "flags", ils ne sont pas utilisés ici
                sck.Send(envoiData, envoiData.Length, SocketFlags.None);
                //Affiche le message envoyé
                listBox_chat_priver.Items.Add("Moi : " + textBoxChatPriver.Text);
                //Efface le contenu de la textbox Envoi
                textBoxChatPriver.Text = "";
            }
            catch
            {
                listBox_chat_priver.Items.Add("Sytème : Impossible d'envoyer le message.");
            }
        }


        private void ReceiveCallBack(IAsyncResult ar)
        {
            //Nouveau tableau de bytes
            byte[] receiveData = new byte[1024];
            //On met les bytes reçus dans notre tableau
            receiveData = (byte[])ar.AsyncState;

            //La classe ASCIIEncoding permet les conversions
            ASCIIEncoding aEncoding = new ASCIIEncoding();
            //conversion bytes -> string
            string receivedMessage = aEncoding.GetString(receiveData);

            //affiche le message
            Mesage(receivedMessage);
            //listBox_chat_priver.Items.Add("Ami(e) : " + receivedMessage);

            //Nouvel objet
            buffer = new byte[1204];
            //redémarrage de l'écoute
            if (!stop_thread)
            {
                try
                {
                    sck.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallBack), buffer);
                }
                catch
                {
                    //listBox_chat_priver.Items.Add("Système : Erreur de réception thread.");
                }
            }
            
        }

        private void Chat_priver_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Arrêt de la réception et de l'envoie sur ce socket
            stop_thread = true;
            try
            {
                sck.Shutdown(SocketShutdown.Both);
                sckEcoute.Shutdown(SocketShutdown.Both);
            }
            catch { }
            try
            {
                sckServeur.Shutdown(SocketShutdown.Both);
            }
            catch { }
            try
            {
                sckClient.Shutdown(SocketShutdown.Both);
            }
            catch { }
            //Fermeture de ce socket
            try
            {
                sck.Close();
            }
            catch { }
            try
            {
                sckEcoute.Close();
            }
            catch { }
            try
            {
                sckServeur.Close();
            }
            catch { }
            try
            {
                sckClient.Close();
            }
            catch { }
        }

        private void Mesage(string a_text)
        //Pour envoyer le mesage entre les thread
        {
            try
            {   
                if (listBox_chat_priver.InvokeRequired && ! stop_thread)
                {
                    Invoke((MethodInvoker)delegate
                    {
                        try
                        {
                            listBox_chat_priver.Items.Add("Ami(e) : " + a_text);
                        }
                        catch
                        {
                            //listBox_chat_priver.Items.Add("Erreur de de thread");
                        }
                    });
                }
            }
            catch { }
        }
    }
}
