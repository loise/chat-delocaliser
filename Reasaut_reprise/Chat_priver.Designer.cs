﻿namespace Reasaut_reprise
{
    partial class Chat_priver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonEnvoiPriver = new System.Windows.Forms.Button();
            this.textBoxChatPriver = new System.Windows.Forms.TextBox();
            this.listBox_chat_priver = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelNomPersonnePriver = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.Controls.Add(this.buttonEnvoiPriver);
            this.panel1.Controls.Add(this.textBoxChatPriver);
            this.panel1.Controls.Add(this.listBox_chat_priver);
            this.panel1.Location = new System.Drawing.Point(49, 84);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(667, 354);
            this.panel1.TabIndex = 0;
            // 
            // buttonEnvoiPriver
            // 
            this.buttonEnvoiPriver.Location = new System.Drawing.Point(544, 315);
            this.buttonEnvoiPriver.Name = "buttonEnvoiPriver";
            this.buttonEnvoiPriver.Size = new System.Drawing.Size(95, 20);
            this.buttonEnvoiPriver.TabIndex = 2;
            this.buttonEnvoiPriver.Text = "button1";
            this.buttonEnvoiPriver.UseVisualStyleBackColor = true;
            this.buttonEnvoiPriver.Click += new System.EventHandler(this.buttonEnvoiPriver_Click);
            // 
            // textBoxChatPriver
            // 
            this.textBoxChatPriver.Location = new System.Drawing.Point(24, 316);
            this.textBoxChatPriver.Name = "textBoxChatPriver";
            this.textBoxChatPriver.Size = new System.Drawing.Size(491, 20);
            this.textBoxChatPriver.TabIndex = 1;
            // 
            // listBox_chat_priver
            // 
            this.listBox_chat_priver.FormattingEnabled = true;
            this.listBox_chat_priver.Location = new System.Drawing.Point(24, 18);
            this.listBox_chat_priver.Name = "listBox_chat_priver";
            this.listBox_chat_priver.Size = new System.Drawing.Size(491, 277);
            this.listBox_chat_priver.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.label1.Location = new System.Drawing.Point(103, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "Chat avec :";
            // 
            // labelNomPersonnePriver
            // 
            this.labelNomPersonnePriver.AutoSize = true;
            this.labelNomPersonnePriver.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.labelNomPersonnePriver.Location = new System.Drawing.Point(250, 30);
            this.labelNomPersonnePriver.Name = "labelNomPersonnePriver";
            this.labelNomPersonnePriver.Size = new System.Drawing.Size(111, 29);
            this.labelNomPersonnePriver.TabIndex = 2;
            this.labelNomPersonnePriver.Text = "persone ";
            // 
            // Chat_priver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelNomPersonnePriver);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "Chat_priver";
            this.Text = "Form2";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Chat_priver_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonEnvoiPriver;
        private System.Windows.Forms.TextBox textBoxChatPriver;
        private System.Windows.Forms.ListBox listBox_chat_priver;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label labelNomPersonnePriver;
    }
}