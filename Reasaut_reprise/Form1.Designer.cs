﻿namespace Reasaut_reprise
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.text_nom_user = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_connection = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_chat_general = new System.Windows.Forms.Panel();
            this.button_envois_general = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.listBox_utilisateur_connecter = new System.Windows.Forms.ListBox();
            this.text_chat_general = new System.Windows.Forms.TextBox();
            this.listBox_chat_general = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.timer_recherche = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel_chat_general.SuspendLayout();
            this.SuspendLayout();
            // 
            // text_nom_user
            // 
            this.text_nom_user.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.text_nom_user.Location = new System.Drawing.Point(130, 23);
            this.text_nom_user.Name = "text_nom_user";
            this.text_nom_user.Size = new System.Drawing.Size(214, 35);
            this.text_nom_user.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.Controls.Add(this.button_connection);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.text_nom_user);
            this.panel1.Location = new System.Drawing.Point(19, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(501, 138);
            this.panel1.TabIndex = 2;
            // 
            // button_connection
            // 
            this.button_connection.Location = new System.Drawing.Point(359, 23);
            this.button_connection.Name = "button_connection";
            this.button_connection.Size = new System.Drawing.Size(125, 34);
            this.button_connection.TabIndex = 2;
            this.button_connection.Text = "Connection";
            this.button_connection.UseVisualStyleBackColor = true;
            this.button_connection.Click += new System.EventHandler(this.button_connection_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.label1.Location = new System.Drawing.Point(29, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nom";
            // 
            // panel_chat_general
            // 
            this.panel_chat_general.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel_chat_general.Controls.Add(this.button_envois_general);
            this.panel_chat_general.Controls.Add(this.label3);
            this.panel_chat_general.Controls.Add(this.listBox_utilisateur_connecter);
            this.panel_chat_general.Controls.Add(this.text_chat_general);
            this.panel_chat_general.Controls.Add(this.listBox_chat_general);
            this.panel_chat_general.Location = new System.Drawing.Point(18, 161);
            this.panel_chat_general.Name = "panel_chat_general";
            this.panel_chat_general.Size = new System.Drawing.Size(1280, 597);
            this.panel_chat_general.TabIndex = 3;
            this.panel_chat_general.Visible = false;
            // 
            // button_envois_general
            // 
            this.button_envois_general.Location = new System.Drawing.Point(1027, 511);
            this.button_envois_general.Name = "button_envois_general";
            this.button_envois_general.Size = new System.Drawing.Size(98, 28);
            this.button_envois_general.TabIndex = 4;
            this.button_envois_general.Text = "Envois";
            this.button_envois_general.UseVisualStyleBackColor = true;
            this.button_envois_general.Click += new System.EventHandler(this.button_envois_general_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.label3.Location = new System.Drawing.Point(953, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(202, 29);
            this.label3.TabIndex = 3;
            this.label3.Text = "Liste d\'utilisateur";
            // 
            // listBox_utilisateur_connecter
            // 
            this.listBox_utilisateur_connecter.FormattingEnabled = true;
            this.listBox_utilisateur_connecter.Location = new System.Drawing.Point(943, 115);
            this.listBox_utilisateur_connecter.Name = "listBox_utilisateur_connecter";
            this.listBox_utilisateur_connecter.Size = new System.Drawing.Size(229, 381);
            this.listBox_utilisateur_connecter.TabIndex = 2;
            this.listBox_utilisateur_connecter.Click += new System.EventHandler(this.listBox_utilisateur_connecter_Click);
            // 
            // text_chat_general
            // 
            this.text_chat_general.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.text_chat_general.Location = new System.Drawing.Point(84, 508);
            this.text_chat_general.Name = "text_chat_general";
            this.text_chat_general.Size = new System.Drawing.Size(853, 29);
            this.text_chat_general.TabIndex = 1;
            // 
            // listBox_chat_general
            // 
            this.listBox_chat_general.FormattingEnabled = true;
            this.listBox_chat_general.Location = new System.Drawing.Point(84, 115);
            this.listBox_chat_general.Name = "listBox_chat_general";
            this.listBox_chat_general.Size = new System.Drawing.Size(853, 381);
            this.listBox_chat_general.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.25F);
            this.label2.Location = new System.Drawing.Point(568, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 44);
            this.label2.TabIndex = 4;
            this.label2.Text = "Chat";
            // 
            // timer_recherche
            // 
            this.timer_recherche.Interval = 5000;
            this.timer_recherche.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1308, 767);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel_chat_general);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel_chat_general.ResumeLayout(false);
            this.panel_chat_general.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox text_nom_user;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel_chat_general;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBox_utilisateur_connecter;
        private System.Windows.Forms.TextBox text_chat_general;
        private System.Windows.Forms.ListBox listBox_chat_general;
        private System.Windows.Forms.Button button_connection;
        private System.Windows.Forms.Button button_envois_general;
        private System.Windows.Forms.Timer timer_recherche;
    }
}

