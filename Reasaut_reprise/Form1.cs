﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net; // pour utiliser les EndPoint
using System.Net.Sockets; //pour utiliser les sockets
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Threading;
using Microsoft.Win32.SafeHandles;

namespace Reasaut_reprise
{
    public partial class Form1 : Form
    {
        Fonctionel fonctionel = new Fonctionel();
        
        public Form1()
        {
            InitializeComponent();
        }

        Fonctionel.Usagers moi = new Fonctionel.Usagers();
        

        //liste qui servira à savoir qui est en ligne
        private List<Fonctionel.Usagers> listeUsers = new List<Fonctionel.Usagers>();
        private List<Fonctionel.Usagers> listeUsersLast = new List<Fonctionel.Usagers>();

        private bool nomValid = true; //est la validation du nom
        private bool connection = true;  // mode confimer le nom devien faux quand le nom est confirmer

        private readonly Random random = new Random();

        //Utiliser pour arreter le tread 
        private bool stop_tread = false;

        private void button_connection_Click(object sender, EventArgs e)
            //Bouton de connection
        {
            //verifer si le nom nes pas utiliser
            if (fonctionel.test_nom_existe(text_nom_user.Text, listeUsers))
            {
                nomValid = true;
                connection = true;
                panel_chat_general.Show();
                moi.Nom = text_nom_user.Text.ToString(); //assigne le nom

                set_ip(); //assigne ip
                text_nom_user.ReadOnly = true;
                lancer_broadcast();
            }
        }


        public void set_ip()
            //Va chercher mon ip
        {
            IPAddress[] localIP = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress address in localIP)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    moi.ip = address.ToString();
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
            //timer recherher les autre
        {
            connection = false;
            
            demandeInfo();
            cant_connect = false;
        }

        //Création des sockets, écoute et envoi
        public Socket sckBoradcastEcoute;
        public Socket sckBoradcastEnvoi;

        public Socket sckEnvoiUDP;

        //Création des EndPoint écoute et envoi
        //Endpoint est "abstract", sert de base pour une autre classe
        EndPoint epBroadcastEcoute, epBroadcastEnvoi;

        //buffer de réception
        byte[] buffer;

        Int32 portBC = 55000;
        Int32 portTCP = 5550;

        //Devient 
        public bool cant_connect = false;


        //--BROADCAST
        public void lancer_broadcast()
        {
            //Définition des sockets. Sockets UDP commme pour le chat UDP
            sckBoradcastEcoute = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sckBoradcastEnvoi = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            //chat general
            sckEnvoiUDP = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            sckEnvoiUDP.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            //epEnvoiUDP = new IPEndPoint(IPAddress, portBC);

            //Configuration de base des sockets
            sckBoradcastEcoute.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            sckBoradcastEnvoi.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, true);

            //connexion du socket de broadcast en écoute
            //IPEndPoint(IPAddress.Any, 55000) signifie que l'on écoute TOUTES les adresses IP
            //qui parlent sur le port 55000
            epBroadcastEcoute = new IPEndPoint(IPAddress.Any, portBC);
            //socket.Bind permet de donner une configuration à notre "bout de connexion"
            sckBoradcastEcoute.Bind(epBroadcastEcoute);

            //Tableau qui permettra la réception des paquets
            buffer = new byte[1024];

            //Début de l'écoute
            try
            {
                sckBoradcastEcoute.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref epBroadcastEcoute, new AsyncCallback(treadBroadcastEcoute), buffer);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString()); //À ajouter pour le débuggage
            }

            //connexion du socket de broadcast en écriture
            //IPEndPoint(IPAddress.Broadcast, 55000) signifie que l'on envoie le paquet
            //à l'adresse de diffusion sur le port 55000
            epBroadcastEnvoi = new IPEndPoint(IPAddress.Broadcast, portBC);
            //socket.Connect permet de donner la configuration du port "à l'autre bout"
            sckBoradcastEnvoi.Connect(epBroadcastEnvoi);

            //affichage du début de connexion dans la console
            listBox_chat_general.Items.Add("Système : Début de conversation");

            //Envoie d'une demande pour trouver les autres usagers
            demandeInfo();

            //démarrage du compteur pour renouveller la liste d'utilisateurs
            timer_recherche.Enabled = true;
        }

        //réception asynchrone du broadcast
        private void treadBroadcastEcoute(IAsyncResult ar)  //--
        {
            if (!stop_tread)
            {
            //Vérifie si le socket est connecté.

                //Invoke permet de créer un "delegate anonyme". Ceci permet de modifier quelque chose gérer par
                //un autre thread (listBox, textBox) asunchrone. Une "copie" est créer et le résultat est poussé
                //au "vrai".
                Invoke((MethodInvoker)delegate
                {
                    try
                    {
                        if (connection | nomValid)
                        {
                            //Création d'un tableau pour traiter le data
                            //le Data reçu est contenu dans l'argument "ar"
                            byte[] receiveData = new byte[1204];
                            //AsyncState correspond au "State" de la fonction sckBoradcastEcoute.BeginReceiveFrom(...)
                            //Finalement, on met "buffer" dans "receiveData"
                            receiveData = (byte[])ar.AsyncState;
                            //Convertion des codes ASCII en string
                            ASCIIEncoding aEncodig = new ASCIIEncoding();
                            string receivedMessage = aEncodig.GetString(receiveData);

                            //Vérifie si le message est une demande de nom, une réponse de nom ou un message
                            //nomReponse sera "true" si le message reçu commence par "Nom:"
                            //nomDemande sera "true" si le message reçu commence par "Qui:"
                            bool nomReponse = receivedMessage.StartsWith("Nom:");
                            bool nomDemande = receivedMessage.StartsWith("Qui:");
                            bool nomConnect = receivedMessage.StartsWith("Connect:");

                            //variable tampon qui sera utilisée pour faire une liste
                            //d'usagers temporaire
                            Fonctionel.Usagers tampon = new Fonctionel.Usagers();

                            //Si le message est une réponse (quelqu'un nous envoi ses info)
                            if (nomReponse)
                            {
                                //nomUtilisateur est un tableau de string
                                //la fonction "Split" permet de séparer les éléments d'une string
                                //en utilisant un caratère (ici, ":")
                                //le résultat sera string[0]= "Nom" et string[1] = le nom de l'utilisateur
                                string[] nomUtilisateur = receivedMessage.Split(':');
                                tampon.Nom = nomUtilisateur[1];
                                tampon.ip = nomUtilisateur[2];
                            
                                //Test pour savoi si notre nom nes pas deja pris
                                if ((moi.Nom == tampon.Nom) &&  connection)
                                {
                                    nomValid = false;
                                }
                                //Ce teste évite d'avoir plusieurs fois le même nom dans la liste.
                                //Plusieur chat peuvent faire la demande "Qui". Ce qui dupliquera les réponses.
                                //Nous ne voulons qu'une seule fois le nom de chaque usager.
                                //Donc, avant de l'ajouter, on regarde s'il y est.
                                if (!(listeUsers.Exists(x => x.Nom == tampon.Nom)))
                                {
                                    listeUsers.Add(new Fonctionel.Usagers() { Nom = tampon.Nom, ip = tampon.ip });
                                }
                            }
                            //Lancer une conversation priver
                            //Celui qui fai le demande envoi son nom et ip qui seron utiliser pour lancer le chat priver
                            else if (nomConnect && !cant_connect)
                            {

                                //Separer le mesage recut pour assigner les donner
                                string[] nomUtilisateur = receivedMessage.Split(':');
                                tampon.Nom = nomUtilisateur[1];
                                tampon.ip = nomUtilisateur[2].Trim().Replace("\0", "");
                                var port_recut = nomUtilisateur[3].Trim().Replace("\0", "");

                                try
                                {
                                    //Etablie la connection avec le endpoint

                                    //Écrit dans le chat que les utilisateur s'ont connecter
                                    listBox_chat_general.Items.Add("connecter");
                                    //Lance le chate priver en envoyant le nom de l’utilisateur et sont ip et si ses le serveur qui le lance ou le client
                                    Chat_priver chat_Priver = new Chat_priver(nomUtilisateur[1], nomUtilisateur[2], int.Parse(port_recut), false);
                                    //Aficher le le chat priver
                                    chat_Priver.moi_ip = moi.ip;
                                    chat_Priver.Show();
                                    //Assigne le socket serveur a celuis de la fenetre priver
                                
                                    chat_Priver.starter_chat(nomUtilisateur[2]);
                                }
                                catch //(Exception ex)
                                {
                                    //MessageBox.Show(ex.ToString()); //pour débogage
                                    listBox_chat_general.Items.Add("Système : Erreur de connexion.");
                                }
                            }
                            //Le message reçu est une demande d'info, on envoie notre nom
                            //avec la fonction "envoiInfo
                            else if (nomDemande && ! connection)
                            {
                                envoiInfo();
                            }
                            //Sinon, c'est que le message est un message
                            else if (!(nomDemande) && !(nomReponse) && !(nomConnect))//Confimer que ses bien un mesage
                            {
                                listBox_chat_general.Items.Add(receivedMessage);
                            }

                            //redémarrage de l'écoute
                            buffer = new byte[1024];
                            try
                            {
                                sckBoradcastEcoute.BeginReceiveMessageFrom(buffer, 0, buffer.Length, SocketFlags.None, ref epBroadcastEcoute, new AsyncCallback(treadBroadcastEcoute), buffer);
                            }
                            catch { }
                        }
                        //Lorseque le nom est déjà utiliser 
                        else
                        {
                            panel_chat_general.Hide();
                            text_nom_user.Text = ""+connection.ToString();
                            text_nom_user.ReadOnly = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.ToString()); //À ajouter pour débuggage
                    }
                });
            }
            
        }

        //Envois les info a propos de l'utilisateur
        private void envoiInfo()    //---
        {
            //envoie du message sur le socket
            try
            {
                sckBoradcastEnvoi.Send(convertire("Nom:" + moi.Nom + ":" + moi.ip+":"));
            }
            catch
            {
                listBox_chat_general.Items.Add("Système : Impossible d'envoyer les info");
            }
        }

        private async void demandeConnection()
        //Fair une demande de connection
        {
            try
            {
                //Donne un port aleatoire a la connection
                var random_number =random.Next(1025, 65000);
                
                //Va chercher la position de l'utilisateur
                var chifre = listBox_utilisateur_connecter.SelectedIndex;
                //Va chercher ip (Le plus 1 sert a ne pas se sélectionner sois même dans la liste)
                IPAddress l_ip = IPAddress.Parse(listeUsersLast[chifre + 1].ip.Replace("\0", ""));

                // cree le form avec le nom de la persone
                //Envoi ip au form et dit qu'il est le serveur
                var nom_contact = listBox_utilisateur_connecter.SelectedItem.ToString();

                //Définition des sockets
                //AdressFamily : protocole d'adressage utilisé (InterNetwork = IPv4)
                //SocketType : type de connection, data tranféré (Stream premet d'envoyer un flux d'information (utilisé pour TCP))
                //ProtocolType : protocole de transport utilisé
                var sckEcoute = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                var sckClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                //sckEcoute.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

                //Définition du EndPoint local pour l'écoute
                //champs : adresse IP, port
                //IPAdresse.Parse(permet de passer d'une string 255.255.255.255 vers le format IPAdress)
                //Convert.ToInt32 permet de convertir une string en int (pour le numéro de port)
                var epLocal = new IPEndPoint(IPAddress.Parse(moi.ip), random_number);
                //association du point de terminaison local du socket
                //socket.Bind permet de donner une configuration à notre "bout de connexion"
                sckEcoute.Bind(epLocal);
                //mettre le socket en écoute, 3 requêtes possibles dans la file, les autres seront rejetés
                //et receveront un message "serveur busy"
                sckEcoute.Listen(3);

                //Informe l'utilisateur du début de l'écoute
                listBox_chat_general.Items.Add("Système : Écoute démarrée.");

                
                //Envois une demande de connection a l'utilisateur connecter
                sckEnvoiUDP.Connect(l_ip, portBC);
                sckEnvoiUDP.Send(convertire("Connect:" + moi.Nom + ":" + moi.ip + ":"+random_number+":"));
                var nom = listBox_utilisateur_connecter.SelectedItem.ToString();
                
                //démarre un Task (Task.Factory.StartNew) asynchrone (await) dans lequel on écoute le socket.
                //Lorsque "Accept()" retournera une réponse (un socket),
                //la réponse sera mise dans sckClient (socket du Client(celui qui fait une requête de connexion)).
                //"() => sckEcoute.Accept())" : méthode exécutée par le task.
                //La valeur retournée, au lieu d'être un task, sera convertie au type de sckClient (Socket)
                sckClient = await Task.Factory.StartNew(() => sckEcoute.Accept());
                //listBox_chat_general.Items.Add("Système : AWait reusi."); //Pour DEBUG
                //instancie le form du chat priver et envoi le nom de la personne a qui on veut parler
                var ChatPriver = new Chat_priver(nom_contact, l_ip.ToString(), random_number, true);
                //Donnne les socket au chat priver
                ChatPriver.sckClient = sckClient;
                ChatPriver.sckServeur = sckClient;//Inutiles logiquement
                ChatPriver.Show();
                ChatPriver.starter_chat(moi.ip);
                try
                {
                    sckEcoute.Close();
                }
                catch
                {
                    listBox_chat_general.Items.Add("Système : fermer socket");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                //throw new Exception(e.ToString());
                listBox_chat_general.Items.Add("Système : Impossible d'envoyer les info (demandeConnection)");
            }
        }

        //Fonction pour demander qui est présent sur le chat
        private void demandeInfo()  ///-----
        {
            //envoie du message sur le socket
            try
            {
                sckBoradcastEnvoi.Send(convertire("Qui:" + text_nom_user.Text));
            }
            catch
            {
                listBox_chat_general.Items.Add("Système : Impossible de faire la demande d'info");
            }

            //À chaque demande, nous mettons à jour la liste
            //Vindange de la liste
            listBox_utilisateur_connecter.Items.Clear();
            //ajout des utilisateurs dans la liste (fenêtre de chat)
            listBox_utilisateur_connecter.BeginUpdate();
            foreach (Fonctionel.Usagers nom in listeUsers)
            {
                if (nom != null)
                {
                    //Vérifie si le nom reçu est le nôtre
                    //Trim() enlève les espace de début et fin
                    //Si ce n'est pas nous, le nom est ajouté à la liste d'usagers
                    //l'argument "flase" indique que nous respectons la casse
                    if (!(String.Compare(moi.Nom, nom.Nom, false) == 0))
                    {
                        listBox_utilisateur_connecter.Items.Add(nom.Nom);
                    }
                }
            }
            listBox_utilisateur_connecter.EndUpdate();

            listeUsersLast = new List<Fonctionel.Usagers>(listeUsers);
            //On vide "listeUsagers" pour recevoir les prochaines info
            listeUsers.Clear();
        }

        public byte[] convertire(string text)
            //Convertie les string en donner qui peuve etre transmise
        {
            //création d'un ASCIIEncoding pour convertir string-ASCII
            ASCIIEncoding aEnconding = new ASCIIEncoding();
            //taille de l'envoie
            byte[] sendingMessage = new byte[1024];
            //conversion des données pour l'envoi
            //Le "Qui:" indiquera que nous voulons connaître les autres usagers

            return sendingMessage = aEnconding.GetBytes(text);
        }

        private void listBox_utilisateur_connecter_Click(object sender, EventArgs e)
        //lorsque clique sur un nom user
        {
            demandeConnection();
        }


        //udp

        public void envoi_mesage()
            //Envoi le mesage ecrit par l'utilisateur
        {
            foreach (var user in listeUsersLast)
            {
                try
                {
                    //listBox_chat_general.Items.Add(user.ip);    //test ecrit l'ip dans le chat
                    sckEnvoiUDP.Connect(IPAddress.Parse(user.ip),portBC);   //Envoi le mesage a chaque utilisatuer selon la liste d<utilisateur
                    sckEnvoiUDP.Send(convertire(moi.Nom + ":" + text_chat_general.Text));
                    
                }
                catch(Exception e)
                {
                    throw new Exception(e.ToString());
                    //listBox_chat_general.Items.Add(e.ToString());
                    
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            stop_tread = true;
            //Ferme les socket
            try
            {
                sckBoradcastEcoute.Shutdown(SocketShutdown.Both);
                sckBoradcastEcoute.Close();
            }
            catch { }
            try
            {
                sckBoradcastEnvoi.Shutdown(SocketShutdown.Both);
                sckBoradcastEnvoi.Close();
            }
            catch { }
            try
            {
                sckEnvoiUDP.Shutdown(SocketShutdown.Both);
                sckEnvoiUDP.Close();
            }
            catch { }
        }

        private void button_envois_general_Click(object sender, EventArgs e)
        {
            envoi_mesage();
        }

    }
}
