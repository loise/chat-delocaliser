﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reasaut_reprise
{
    class Fonctionel
    {

        //Classe usager
        public class Usagers
        {
            public string Nom;
            public string ip;
        }


        //Tester si le nom est pas le même que dans la list usager
        public bool test_nom_existe(string nom, List<Usagers> listeUsers)
        {
            bool existe_pas = true;

            foreach (var usager in listeUsers)
            {
                if (usager.Nom == nom)
                {
                    existe_pas = false;
                }
            }
            return existe_pas;
        }

    }
}
